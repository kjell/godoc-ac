# auto-complete for godoc

I wanted some more convenience when using godoc from command line, so
this is an attempt to solve this using bash completion.


![demo](demo.gif)

## Install


    # download
    curl https://gitlab.com/kjell/godoc-ac/raw/master/godoc > godoc

    # install
    sudo cp godoc /etc/bash_completion.d/

    # reload
    source ~/.bash

test typing `godocTAB!TAB!`

`TAB!` - means push the tab button :)
